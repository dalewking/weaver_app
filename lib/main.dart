import 'package:flutter/material.dart';

import 'di/injection.dart';

void main() => runApp(iocContainer<MaterialApp>());
