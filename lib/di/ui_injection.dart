import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:jsonplaceholder_api/api.dart';
import 'package:redux/redux.dart';

import '../api/navigation.dart' as api;
import '../api/state.dart' as api;
import '../ui/ui.dart' as ui;

final themeData = ThemeData(
  primarySwatch: Colors.blue,
);

final commentPath = RegExp("^/comments/(\\d+)\$");

extension Configuration on GetIt {
  void configureUi() {
    registerSingleton(GlobalKey<NavigatorState>());
    registerSingleton<api.Navigation>(ui.Navigation(this()));

    registerSingleton(
      MaterialApp(
        navigatorKey: this(),
        theme: themeData,
        debugShowCheckedModeBanner: false,
        initialRoute: "/posts",
        routes: {"/posts": (context) => ui.PostList(this())},
        onGenerateRoute: routeFactory,
      ),
    );
  }

  Route routeFactory(settings) {
    switch (settings.name) {
      case '/createPost':
        return ui.DialogRoute<Post>(
          pageBuilder: (context, _, __) => ui.CreatePost(this()),
          barrierDismissible: false,
        );
      case '/savingPost':
        return ui.DialogRoute(
          pageBuilder: (context, _, __) => ui.BusyOverlay("Saving Post"),
          barrierDismissible: false,
        );
      case '/error':
        return ui.DialogRoute(
          pageBuilder: (context, _, __) => ui.ErrorDialog(settings.arguments, this()),
          barrierDismissible: false,
        );
      default:
        var match = commentPath.firstMatch(settings.name);
        if (match != null) {
          int postId = int.parse(match.group(1));

          return MaterialPageRoute(
            settings: settings,
            builder: (context) => ui.CommentList(this<Store<api.CommentList> Function(int)>()(postId)),
          );
        } else {
          return null;
        }
    }
  }
}
