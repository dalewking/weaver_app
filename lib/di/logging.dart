import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';

extension Configuration on GetIt {
  void configureLogging() {
    hierarchicalLoggingEnabled = true;

    Logger.root.onRecord.listen((LogRecord rec) =>
        print('${rec.level.name.padRight(7)}: ${rec.time}: ${rec.loggerName}: ${rec.message}'));

    Logger("redux").level = Level.WARNING;
    Logger("redux.CreatePost").level = null;
    Logger("redux.PostList").level = null;
    Logger("redux.CommentList").level = null;
  }
}
