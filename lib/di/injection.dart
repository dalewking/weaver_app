import 'package:get_it/get_it.dart';

import 'api_injection.dart';
import 'logging.dart';
import 'redux_injection.dart';
import 'ui_injection.dart';

final iocContainer = GetIt.instance
  ..configureLogging()
  ..configureApi()
  ..configureRedux()
  ..configureUi();
