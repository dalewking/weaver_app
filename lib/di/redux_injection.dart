import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';

import '../api/actions.dart';
import '../api/navigation.dart' as api;
import '../api/state.dart' as api;
import '../redux/comment_list.dart' as comment_list;
import '../redux/create_post.dart' as create_post;
import '../redux/post_list.dart' as post_list;

extension Configuration on GetIt {
  void configureRedux() {
    configurePostList();
    configureCommentList();
    configureCreatePost();
  }

  static String logFormatter(
    dynamic state,
    dynamic action,
    DateTime timestamp,
  ) {
    return "Action: $action,\n" + " State: $state";
  }

  void configurePostList() => registerLazySingleton<Store<api.PostList>>(
        () => Store<post_list.State>(
          post_list.reducer,
          initialState: post_list.State(),
          middleware: [
            LoggingMiddleware(logger: Logger("redux.PostList"), formatter: logFormatter),
            post_list.MiddlewareHandler(this(), this()),
          ],
        )..dispatch(Refresh()),
      );

  void configureCreatePost() => registerFactory<Store<api.CreatePost>>(
        () => Store<create_post.State>(
          create_post.reducer,
          initialState: create_post.State(),
          middleware: [
            LoggingMiddleware(logger: Logger("redux.CreatePost"), formatter: logFormatter),
            create_post.MiddlewareHandler(this(), this()),
          ],
        ),
      );

  void configureCommentList() =>
      registerSingleton<Store<api.CommentList> Function(int)>((i) => Store<comment_list.State>(
            comment_list.reducer,
            initialState: comment_list.State(i),
            middleware: [
              LoggingMiddleware(logger: Logger("redux.CommentList"), formatter: logFormatter),
              comment_list.MiddlewareHandler(this(), this()),
            ],
          )..dispatch(Refresh()));
}
