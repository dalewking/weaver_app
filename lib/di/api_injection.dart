import 'package:get_it/get_it.dart';
import 'package:jsonplaceholder_api/api.dart';

extension Configuration on GetIt {
  void configureApi() {
    registerLazySingleton(() => ApiClient());
    registerLazySingleton(() => PostsApi(this()));
    registerLazySingleton(() => CommentsApi(this()));
  }
}
