import 'package:jsonplaceholder_api/api.dart';
import 'package:weaver_app/api/actions.dart';
import 'package:weaver_app/redux/actions.dart';

import '../../api/state.dart' as api;
import 'state.dart';

extension on StateBuilder {
  void reduce(dynamic action) {
    if (action is SetTitle) {
      title = action.parameter;
    } else if (action is SetBody) {
      body = action.parameter;
    } else if (action is api.Result<Post>) {
      created = action;
    }
  }
}

State reducer(State state, dynamic action) => (state.toBuilder()..reduce(action)).build();
