import 'package:built_value/built_value.dart';
import 'package:jsonplaceholder_api/api.dart';

import '../../api/state.dart' as api;
import '../../api/state.dart' show Result;

part 'state.g.dart';

abstract class State implements api.CreatePost, Built<State, StateBuilder> {
  String get title;
  String get body;

  bool isValidTitle(String title) => title?.trim()?.isNotEmpty ?? false;
  bool isValidBody(String body) => body?.trim()?.isNotEmpty ?? false;

  State._();

  factory State() => _$State._(title: "", body: "", created: api.Result());
}
