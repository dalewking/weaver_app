import 'package:jsonplaceholder_api/api.dart';
import 'package:redux/redux.dart';
import 'package:weaver_app/redux/actions.dart';

import '../../api/actions.dart';
import '../../api/navigation.dart';
import 'state.dart';

class MiddlewareHandler extends MiddlewareClass<State> {
  final PostsApi api;
  final Navigation navigation;

  MiddlewareHandler(this.api, this.navigation);

  @override
  dynamic call(Store<State> store, dynamic action, NextDispatcher next) async {
    if (action is SavePost) {
      if (store.state.isValidTitle(store.state.title) && store.state.isValidBody(store.state.body)) {
        next(store.state.created.starting());
        navigation.showSavingPost();

        try {
          Post post = await api.addPost(
              newPost: NewPost()
                ..title = store.state.title
                ..body = store.state.body
                ..userId = 1);

          navigation.pop();
          navigation.pop(post);
          store.dispatch(store.state.created.succeeded(post));
        } catch (e) {
          navigation.pop();
          navigation.showError("Failure saving post");
          store.dispatch(store.state.created.failed(e));
        }
      }

      return;
    }
    next(action);
  }
}
