// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$State extends State {
  @override
  final Post successfullyCreated;
  @override
  final Result<List<Post>> result;

  factory _$State([void Function(StateBuilder) updates]) =>
      (new StateBuilder()..update(updates)).build();

  _$State._({this.successfullyCreated, this.result}) : super._() {
    if (result == null) {
      throw new BuiltValueNullFieldError('State', 'result');
    }
  }

  @override
  State rebuild(void Function(StateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StateBuilder toBuilder() => new StateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is State &&
        successfullyCreated == other.successfullyCreated &&
        result == other.result;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, successfullyCreated.hashCode), result.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('State')
          ..add('successfullyCreated', successfullyCreated)
          ..add('result', result))
        .toString();
  }
}

class StateBuilder implements Builder<State, StateBuilder> {
  _$State _$v;

  Post _successfullyCreated;
  Post get successfullyCreated => _$this._successfullyCreated;
  set successfullyCreated(Post successfullyCreated) =>
      _$this._successfullyCreated = successfullyCreated;

  Result<List<Post>> _result;
  Result<List<Post>> get result => _$this._result;
  set result(Result<List<Post>> result) => _$this._result = result;

  StateBuilder();

  StateBuilder get _$this {
    if (_$v != null) {
      _successfullyCreated = _$v.successfullyCreated;
      _result = _$v.result;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(State other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$State;
  }

  @override
  void update(void Function(StateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$State build() {
    final _$result = _$v ??
        new _$State._(successfullyCreated: successfullyCreated, result: result);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
