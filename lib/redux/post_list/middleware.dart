import 'package:jsonplaceholder_api/api.dart';
import 'package:redux/redux.dart';
import 'package:weaver_app/api/navigation.dart';

import '../actions.dart';
import 'state.dart';

class MiddlewareHandler extends MiddlewareClass<State> {
  final PostsApi api;
  final Navigation navigation;

  MiddlewareHandler(this.api, this.navigation);

  @override
  dynamic call(Store<State> store, dynamic action, NextDispatcher next) async {
    if (action is Refresh) {
      next(store.state.result.starting());

      try {
        store.dispatch(store.state.result.succeeded(await api.getPosts()));
      } catch (e) {
        navigation.showError("Failure loading posts");
        store.dispatch(store.state.result.failed(e));
      }

      return;
    } else if (action is ShowComments) {
      navigation.showComments(action.parameter);
    } else if (action is CreatePost) {
      Post result = await navigation.createPost();

      if (result != null) {
        next(ShowSaveSuccess(result));
      }

      next(ShowSaveSuccess(null));
    }

    next(action);
  }
}
