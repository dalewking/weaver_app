import 'package:jsonplaceholder_api/api.dart';

import '../../api/state.dart' as api;
import '../actions.dart';
import 'state.dart';

extension on StateBuilder {
  void reduce(dynamic action) {
    if (action is api.Result<List<Post>>) {
      result = action;
    } else if (action is ShowSaveSuccess) {
      successfullyCreated = action.parameter;
    }
  }
}

State reducer(State state, dynamic action) => (state.toBuilder()..reduce(action)).build();
