import 'package:built_value/built_value.dart';
import 'package:jsonplaceholder_api/api.dart';

import '../../api/state.dart' as api;
import '../../api/state.dart' show Result;

part 'state.g.dart';

abstract class State implements api.PostList, Built<State, StateBuilder> {
  State._();

  @nullable
  Post get successfullyCreated;

  factory State() => _$State._(result: api.Result([]));
}
