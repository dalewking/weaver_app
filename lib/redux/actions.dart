import 'package:jsonplaceholder_api/api.dart';

import '../api/actions.dart';

export '../api/actions.dart';

class ShowSaveSuccess extends Action<Post> {
  ShowSaveSuccess(Post post) : super(post);
}
