import 'package:built_value/built_value.dart';
import 'package:jsonplaceholder_api/api.dart';

import '../../api/state.dart' as api;
import '../../api/state.dart' show Result;

part 'state.g.dart';

abstract class State implements api.CommentList, Built<State, StateBuilder> {
  int get postId;

  State._();

  factory State(int postId) => _$State._(postId: postId, result: api.Result([]));
}
