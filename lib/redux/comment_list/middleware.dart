import 'package:jsonplaceholder_api/api.dart';
import 'package:redux/redux.dart';
import 'package:weaver_app/api/navigation.dart';

import '../actions.dart';
import 'state.dart';

class MiddlewareHandler extends MiddlewareClass<State> {
  final CommentsApi api;
  final Navigation navigation;

  MiddlewareHandler(this.api, this.navigation);

  @override
  dynamic call(Store<State> store, dynamic action, NextDispatcher next) async {
    if (action is Refresh) {
      next(store.state.result.starting());

      try {
        store.dispatch(store.state.result.succeeded(await api.getComments(postId: store.state.postId)));
      } catch (e) {
        navigation.showError("Failure loading comments");
        store.dispatch(store.state.result.failed(e));
      }
    }

    next(action);
  }
}
