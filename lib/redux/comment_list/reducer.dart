import 'package:jsonplaceholder_api/api.dart';

import '../../api/state.dart' as api;
import 'state.dart';

extension on StateBuilder {
  void reduce(dynamic action) {
    if (action is api.Result<List<Comment>>) {
      result = action;
    }
  }
}

State reducer(State state, dynamic action) => (state.toBuilder()..reduce(action)).build();
