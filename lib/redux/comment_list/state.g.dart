// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$State extends State {
  @override
  final int postId;
  @override
  final Result<List<Comment>> result;

  factory _$State([void Function(StateBuilder) updates]) =>
      (new StateBuilder()..update(updates)).build();

  _$State._({this.postId, this.result}) : super._() {
    if (postId == null) {
      throw new BuiltValueNullFieldError('State', 'postId');
    }
    if (result == null) {
      throw new BuiltValueNullFieldError('State', 'result');
    }
  }

  @override
  State rebuild(void Function(StateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StateBuilder toBuilder() => new StateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is State && postId == other.postId && result == other.result;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, postId.hashCode), result.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('State')
          ..add('postId', postId)
          ..add('result', result))
        .toString();
  }
}

class StateBuilder implements Builder<State, StateBuilder> {
  _$State _$v;

  int _postId;
  int get postId => _$this._postId;
  set postId(int postId) => _$this._postId = postId;

  Result<List<Comment>> _result;
  Result<List<Comment>> get result => _$this._result;
  set result(Result<List<Comment>> result) => _$this._result = result;

  StateBuilder();

  StateBuilder get _$this {
    if (_$v != null) {
      _postId = _$v.postId;
      _result = _$v.result;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(State other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$State;
  }

  @override
  void update(void Function(StateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$State build() {
    final _$result = _$v ?? new _$State._(postId: postId, result: result);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
