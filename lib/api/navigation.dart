import 'package:jsonplaceholder_api/api.dart';

abstract class Navigation {
  void showComments(int postId);

  Future<Post> createPost();

  void showSavingPost();

  void showError(String message);

  void pop([dynamic result]);
}
