import 'package:jsonplaceholder_api/api.dart';

class Result<T> {
  final T data;
  final bool executing;
  final Exception error;

  Result._(this.data, {this.error, this.executing = false});

  factory Result([T data]) => Result._(data);

  Result<T> starting() => Result._(data, executing: true);
  Result<T> succeeded(T data) => Result._(data);
  Result<T> failed(Exception error, [T data]) => Result._(data ?? this.data, error: error);

  @override
  String toString() {
    if (executing) {
      return "Result(loading)";
    } else if (error != null) {
      return "Result(error: $error)";
    } else {
      return "Result(succeeded: $data)";
    }
  }
}

abstract class PostList {
  Result<List<Post>> get result;
  Post get successfullyCreated;
}

abstract class CommentList {
  Result<List<Comment>> get result;
}

abstract class CreatePost {
  bool isValidTitle(String title);
  bool isValidBody(String body);

  Result<Post> get created;
}
