import 'package:flutter/foundation.dart';

abstract class Action<T> {
  final T parameter;

  const Action(this.parameter);

  toString() => "${this.runtimeType.toString()}($parameter)";
}

abstract class EmptyAction extends Action<Null> {
  const EmptyAction() : super(null);

  toString() => this.runtimeType.toString();
}

class Refresh extends EmptyAction {}

class ShowComments extends Action<int> {
  ShowComments({@required int postId}) : super(postId);
}

class CreatePost extends EmptyAction {}

class SetTitle extends Action<String> {
  SetTitle(String title) : super(title);
}

class SetBody extends Action<String> {
  SetBody(String title) : super(title);
}

class SavePost extends EmptyAction {}
