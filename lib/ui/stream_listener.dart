import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class StreamListener<T> extends StatefulWidget {
  final Function(BuildContext, T data) listener;

  final Stream<T> stream;

  final Widget child;

  StreamListener({
    Key key,
    @required this.stream,
    @required this.child,
    @required this.listener,
  }) : super(key: key);

  @override
  State<StreamListener<T>> createState() => _StreamListenerState<T>();
}

/// State for [StreamBuilderBase].
class _StreamListenerState<T> extends State<StreamListener<T>> {
  StreamSubscription<T> _subscription;

  @override
  void initState() {
    super.initState();
    _subscribe();
  }

  @override
  void didUpdateWidget(StreamListener<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.stream != widget.stream) {
      if (_subscription != null) {
        _unsubscribe();
      }
      _subscribe();
    }
  }

  @override
  Widget build(BuildContext context) => widget.child;

  @override
  void dispose() {
    _unsubscribe();
    super.dispose();
  }

  void _subscribe() {
    if (widget.stream != null) {
      _subscription = widget.stream.listen((T data) {
        print("Calling Listener $data");
        widget.listener(context, data);
      });
    }
  }

  void _unsubscribe() {
    if (_subscription != null) {
      _subscription.cancel();
      _subscription = null;
    }
  }
}
