import 'package:flutter/material.dart';

class ErrorDialog extends AlertDialog {
  ErrorDialog(String message, GlobalKey<NavigatorState> navigator)
      : super(
          title: Text(
            "Error",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: Text(message),
          actions: [
            FlatButton(
              child: new Text("OK"),
              onPressed: () => navigator.currentState.pop(),
            ),
          ],
        );
}
