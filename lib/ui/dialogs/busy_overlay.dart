import 'package:flutter/material.dart';

class BusyOverlay extends AlertDialog {
  BusyOverlay(String message)
      : super(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(message),
              SizedBox(height: 8),
              CircularProgressIndicator(),
            ],
          ),
        );
}
