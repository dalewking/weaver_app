import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:weaver_app/api/actions.dart';

import '../../api/state.dart' as api;

class CreatePost extends StatefulWidget {
  final Store<api.CreatePost> store;

  CreatePost(this.store);

  @override
  _CreatePostState createState() => _CreatePostState();
}

class _CreatePostState extends State<CreatePost> {
  final _formKey = GlobalKey<FormState>();

  Function(dynamic) get dispatch => widget.store.dispatch;

  final FocusNode titleFocus = FocusNode();
  final FocusNode bodyFocus = FocusNode();

  @override
  Widget build(BuildContext context) => AlertDialog(
        title: const Text(
          "Create Post",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        content: _createForm(context),
        actions: <Widget>[
          FlatButton(
            child: const Text("CANCEL"),
            onPressed: () => Navigator.of(context).pop(null),
          ),
          FlatButton(
            child: const Text("OK"),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                dispatch(SavePost());
              }
              ;
            },
          ),
        ],
      );

  Widget _makeTextField(
    BuildContext context, {
    @required FocusNode focus,
    FocusNode nextFocus,
    @required String hint,
    String validationText,
    TextCapitalization capitalization = TextCapitalization.words,
    int maxLines,
    @required bool Function(String) validator,
    @required FormFieldSetter<String> setter,
  }) {
    return TextFormField(
      controller: TextEditingController(),
      maxLines: maxLines,
      textCapitalization: capitalization,
      decoration: InputDecoration(border: OutlineInputBorder(), hintText: hint),
      validator: (value) {
        if (!validator(value)) {
          return validationText ?? "Please enter ${hint.toLowerCase()}";
        }

        return null;
      },
      textInputAction: nextFocus != null ? TextInputAction.next : TextInputAction.done,
      focusNode: focus,
      onSaved: setter,
      onFieldSubmitted: (t) {
        focus.unfocus();
        if (nextFocus != null) {
          FocusScope.of(context).requestFocus(nextFocus);
        }
      },
    );
  }

  Widget _createForm(BuildContext context) => Form(
        key: _formKey,
        onChanged: () => _formKey.currentState.save(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _makeTextField(
              context,
              focus: titleFocus,
              nextFocus: bodyFocus,
              hint: "Title",
              maxLines: 1,
              validator: widget.store.state.isValidTitle,
              setter: (s) => dispatch(SetTitle(s)),
            ),
            SizedBox(height: 10),
            _makeTextField(
              context,
              focus: bodyFocus,
              hint: "Body",
              maxLines: 10,
              validator: widget.store.state.isValidBody,
              setter: (s) => dispatch(SetBody(s)),
            ),
          ],
        ),
      );
}
