import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jsonplaceholder_api/api.dart';
import 'package:redux/redux.dart';

import '../../api/actions.dart';
import '../../api/state.dart' as api;

class CommentList extends StatelessWidget {
  final Store<api.CommentList> store;

  Function(dynamic) get dispatch => store.dispatch;

  CommentList(this.store, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("Comments"),
          actions: <Widget>[_refreshWidget()],
        ),
        body: StreamBuilder<api.Result<List<Comment>>>(
          stream: store.onChange.map((s) => s.result),
          initialData: api.Result([]),
          builder: (context, snapshot) => Stack(
            children: [
              _buildCommentList(snapshot.data.data),
              if (snapshot.data.executing)
                Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                )
            ],
          ),
        ),
      );

  Widget _buildCommentList(List<Comment> comments) => Scrollbar(
        child: ListView.separated(
          itemCount: comments.length,
          itemBuilder: (context, i) => _buildCommentItem(comments[i]),
          separatorBuilder: (context, index) => Divider(
            thickness: 1.0,
            color: Colors.black87,
          ),
        ),
      );

  Widget _refreshWidget() => StreamBuilder(
        stream: store.onChange.map((s) => s.result.executing),
        initialData: true,
        builder: (context, snapshot) => IconButton(
          icon: const Icon(Icons.refresh),
          tooltip: "Refresh",
          onPressed: snapshot.data ? null : () => dispatch(Refresh()),
        ),
      );

  Widget _buildCommentItem(Comment comment) => ListTile(
        key: ValueKey(comment.id),
        title: Text("${comment.email} - ${comment.name}"),
        subtitle: Text(comment.body),
      );
}
