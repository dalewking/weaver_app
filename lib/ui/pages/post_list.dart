import 'package:flutter/material.dart';
import 'package:jsonplaceholder_api/api.dart';
import 'package:redux/redux.dart';
import 'package:weaver_app/ui/stream_listener.dart';

import '../../api/actions.dart';
import '../../api/state.dart' as api;

class PostList extends StatelessWidget {
  final Store<api.PostList> store;

  Function(dynamic) get dispatch => store.dispatch;

  PostList(this.store, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("Posts"),
          actions: <Widget>[_refreshWidget()],
        ),
        body: StreamListener<Post>(
          stream: store.onChange.map((s) => s.successfullyCreated).where((p) => p != null),
          listener: (context, created) {
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text(
                "Post saved with record id ${created.id}",
                textAlign: TextAlign.center,
              ),
            ));
          },
          child: StreamBuilder<api.Result<List<Post>>>(
            stream: store.onChange.map((s) => s.result),
            initialData: api.Result([]),
            builder: (context, snapshot) {
              return Stack(
                children: [
                  _buildPostList(snapshot.data.data),
                  if (snapshot.data.executing) Center(child: CircularProgressIndicator()),
                ],
              );
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => dispatch(CreatePost()),
          tooltip: 'Create Post',
          child: Icon(Icons.add),
        ),
      );

  Widget _buildPostList(List<Post> posts) => Scrollbar(
        child: ListView.separated(
          itemCount: posts.length,
          itemBuilder: (context, i) => _buildPostItem(posts[i]),
          separatorBuilder: (context, index) => Divider(
            thickness: 1.0,
            color: Colors.black87,
          ),
        ),
      );

  Widget _refreshWidget() => StreamBuilder<bool>(
        stream: store.onChange.map((s) => s.result.executing),
        initialData: true,
        builder: (context, snapshot) => IconButton(
          icon: const Icon(Icons.refresh),
          tooltip: "Refresh",
          onPressed: snapshot.data ? null : () => dispatch(Refresh()),
        ),
      );

  Widget _buildPostItem(Post post) => ListTile(
        key: ValueKey(post.id),
        title: Text(post.title),
        subtitle: Text(post.body),
        onTap: () => dispatch(ShowComments(postId: post.id)),
      );
}
