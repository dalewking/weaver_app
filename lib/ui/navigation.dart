import 'package:flutter/widgets.dart';
import 'package:jsonplaceholder_api/api.dart';

import '../api/navigation.dart' as api;

class Navigation implements api.Navigation {
  final GlobalKey<NavigatorState> navigation;

  Navigation(this.navigation);

  @override
  void showComments(int postId) => navigation.currentState.pushNamed("/comments/$postId");

  @override
  Future<Post> createPost() => navigation.currentState.pushNamed("/createPost");

  @override
  void showSavingPost() => navigation.currentState.pushNamed("/savingPost");

  @override
  void pop([dynamic result]) => navigation.currentState.pop(result);

  @override
  void showError(String message) => navigation.currentState.pushNamed("/error", arguments: message);
}
