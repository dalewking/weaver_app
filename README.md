# weaver_app

Example app for interview process

## Running the application

This software can be run on a variety of devices including:
- an iOS device, requires some setup which is up to you to configure
- the iOS simulator 
- an attached Android device that is in
  [developer mode](https://developer.android.com/studio/debug/dev-options)  
- a running Android emulator
- as a MacOS desktop application
- in Google Chrome
- as a web server

Please note that the last 3 devices are still experimental, and may not
behave as well as iOS or Android devices. In particular for Chrome,
there are no snack bars.

To see the options available to you, open a shell to the root of the
project and issue the command `./flutterw devices`. Here is example
output from my mac:
```
    SM N975U                  • RF8M91N694R                          • android-arm64  • Android 9 (API 28)
    Android SDK built for x86 • emulator-5554                        • android-x86    • Android 8.1.0 (API 27) (emulator)
    iPhone 11 Pro Max         • 1C91A903-0F7C-44D4-B170-CFA11DE787EF • ios            • com.apple.CoreSimulator.SimRuntime.iOS-13-3 (simulator)
    macOS                     • macOS                                • darwin-x64     • Mac OS X 10.14.6 18G1012
    Chrome                    • chrome                               • web-javascript • Google Chrome 78.0.3904.108
    Web Server                • web-server                           • web-javascript • Flutter Tools
```

To run the application on one of those devices you can use
`./flutterw run -d <device>` where `<device>` is a unique prefix
from the values in either of the first 2 columns. E.g. I
could say `./flutterw run -d SM` to run it on my Samsung phone.

## Problem statement

App displays a list of text posts. The user should be able to tap on
 a post to see additional comments related to the post. The user
 should also be able to create a text post via a form and be notified
 of its success.

API resource: [https://jsonplaceholder.typicode.com/guide.html](https://jsonplaceholder.typicode.com/guide.html)

1. Create a new Flutter App for IOS and Android
2. Use the Flutter Material module/pattern
3. Create a View which shows a list view
    - Include a Button to invoke the api call
    - On button tap, use this public json api
        - GET [https://jsonplaceholder.typicode.com/posts](https://jsonplaceholder.typicode.com/posts)
    - Display each record's title and body in the json as a row/card
    - Make each row tappable
    - On Tap of the row, use the “id” field of the row to hit json api
        - GET https://jsonplaceholder.typicode.com/comments?postId=\[id\]
    - Open a new view
4. Create a Second View
    - Include a Back Button
    - Pass the results of the comments call into a List view
    - Display the name, email, and body fields in the list view row
5. Back on View 1
    - Add a Create Button
    - On Tap, open a Modal with the Form  fields Title and Body and a OK button
    - On OK tap, make a POST call to https://jsonplaceholder.typicode.com/posts
       - Model { title: “”, body: “”, userId: 1}
    - In the api success callback,
        - Close the modal
        - display a snackbar of “Post saved with record id \[result.id\]”
6. App appearance is a nice to have but not critical

## Development Notes:
- The project was done in a series of very small individual commits each
  modifying only one thing so the git commits tell the cohesive story of
  building the app.
- I used the [Flutter Wrapper](https://github.com/passsy/flutter_wrapper)
  which allows you to control flutter version in each repo instead of
  globally on the computer
    - Instead of using `flutter` command you use `./flutterw`
    - If opening in an IDE there is additional
     [IDE setup](https://github.com/passsy/flutter_wrapper#ide-setup)
     that is needed 
- Rather than generating code for dealing with JSON API by hand I prefer
  to generate the code from an [OpenApi](https://www.openapis.org/)
  specification using [OpenApi-Generator](https://openapi-generator.tech/)
  - This is faster, and much less error-prone
  - It also generates nice documentation as can be seen 
    [here](jsonplaceholder_api/README.md)
  - To do the generation easily added a
   [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) 
   to downnload and run the [openapi-generator-plugin](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-gradle-plugin)
   - The API code is generated with the command:
     - `./gradlew apiGenerate`
- I used a very simple IOC container, get_it. I have been exploring
  other choices but went with this very straight-forward one for this
  project.
- For state management I chose to use [redux](https://pub.dev/packages/redux) .
  I am exploring alternatives to redux in my personal projects, but for
  this project I used redux as that is what I am most familiar with.
