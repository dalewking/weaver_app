# jsonplaceholder_api.api.UsersApi

## Load the API package
```dart
import 'package:jsonplaceholder_api/api.dart';
```

All URIs are relative to *https://jsonplaceholder.typicode.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUser**](UsersApi.md#getUser) | **GET** /users/{id} | Get specific user
[**getUsers**](UsersApi.md#getUsers) | **GET** /users | Get all available users


# **getUser**
> User getUser(id)

Get specific user

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = UsersApi();
var id = 56; // int | The ID of the user to retrieve

try { 
    var result = api_instance.getUser(id);
    print(result);
} catch (e) {
    print("Exception when calling UsersApi->getUser: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the user to retrieve | [default to null]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getUsers**
> List<User> getUsers(id, email)

Get all available users

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = UsersApi();
var id = 56; // int | Filter by user ID
var email = 56; // int | Filter by user email address

try { 
    var result = api_instance.getUsers(id, email);
    print(result);
} catch (e) {
    print("Exception when calling UsersApi->getUsers: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Filter by user ID | [optional] [default to null]
 **email** | **int**| Filter by user email address | [optional] [default to null]

### Return type

[**List<User>**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

