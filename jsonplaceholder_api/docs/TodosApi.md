# jsonplaceholder_api.api.TodosApi

## Load the API package
```dart
import 'package:jsonplaceholder_api/api.dart';
```

All URIs are relative to *https://jsonplaceholder.typicode.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTodo**](TodosApi.md#getTodo) | **GET** /todos/{id} | Get specific todo
[**getTodos**](TodosApi.md#getTodos) | **GET** /todos | Get all available todos


# **getTodo**
> Todo getTodo(id)

Get specific todo

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = TodosApi();
var id = 56; // int | The ID of the todo to retrieve

try { 
    var result = api_instance.getTodo(id);
    print(result);
} catch (e) {
    print("Exception when calling TodosApi->getTodo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the todo to retrieve | [default to null]

### Return type

[**Todo**](Todo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTodos**
> List<Todo> getTodos(id, userId)

Get all available todos

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = TodosApi();
var id = 56; // int | Filter by todo ID
var userId = 56; // int | Filter by user ID

try { 
    var result = api_instance.getTodos(id, userId);
    print(result);
} catch (e) {
    print("Exception when calling TodosApi->getTodos: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Filter by todo ID | [optional] [default to null]
 **userId** | **int**| Filter by user ID | [optional] [default to null]

### Return type

[**List<Todo>**](Todo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

