# jsonplaceholder_api.api.CommentsApi

## Load the API package
```dart
import 'package:jsonplaceholder_api/api.dart';
```

All URIs are relative to *https://jsonplaceholder.typicode.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getComment**](CommentsApi.md#getComment) | **GET** /comments/{id} | Get specific comment
[**getComments**](CommentsApi.md#getComments) | **GET** /comments | Get all available comments


# **getComment**
> Comment getComment(id)

Get specific comment

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = CommentsApi();
var id = 56; // int | The ID of the comment to retrieve

try { 
    var result = api_instance.getComment(id);
    print(result);
} catch (e) {
    print("Exception when calling CommentsApi->getComment: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the comment to retrieve | [default to null]

### Return type

[**Comment**](Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getComments**
> List<Comment> getComments(id, postId)

Get all available comments

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = CommentsApi();
var id = 56; // int | Filter by comment ID
var postId = 56; // int | Filter by post ID

try { 
    var result = api_instance.getComments(id, postId);
    print(result);
} catch (e) {
    print("Exception when calling CommentsApi->getComments: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Filter by comment ID | [optional] [default to null]
 **postId** | **int**| Filter by post ID | [optional] [default to null]

### Return type

[**List<Comment>**](Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

