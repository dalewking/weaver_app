# jsonplaceholder_api.api.PostsApi

## Load the API package
```dart
import 'package:jsonplaceholder_api/api.dart';
```

All URIs are relative to *https://jsonplaceholder.typicode.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addPost**](PostsApi.md#addPost) | **POST** /posts | Create new posts
[**getPosts**](PostsApi.md#getPosts) | **GET** /posts | Get all available posts
[**postsIdCommentsGet**](PostsApi.md#postsIdCommentsGet) | **GET** /posts/{id}/comments | Get comments for a specific post
[**postsIdGet**](PostsApi.md#postsIdGet) | **GET** /posts/{id} | Get specific post


# **addPost**
> Post addPost(newPost)

Create new posts

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = PostsApi();
var newPost = NewPost(); // NewPost | 

try { 
    var result = api_instance.addPost(newPost);
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->addPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **newPost** | [**NewPost**](NewPost.md)|  | [optional] 

### Return type

[**Post**](Post.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPosts**
> List<Post> getPosts(id, userId)

Get all available posts

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = PostsApi();
var id = 56; // int | Filter by post ID
var userId = 56; // int | Filter by user ID

try { 
    var result = api_instance.getPosts(id, userId);
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->getPosts: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Filter by post ID | [optional] [default to null]
 **userId** | **int**| Filter by user ID | [optional] [default to null]

### Return type

[**List<Post>**](Post.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postsIdCommentsGet**
> List<Comment> postsIdCommentsGet(id)

Get comments for a specific post

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = PostsApi();
var id = 56; // int | post id

try { 
    var result = api_instance.postsIdCommentsGet(id);
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->postsIdCommentsGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| post id | [default to null]

### Return type

[**List<Comment>**](Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postsIdGet**
> Post postsIdGet(id)

Get specific post

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = PostsApi();
var id = 56; // int | The ID of the post to retrieve

try { 
    var result = api_instance.postsIdGet(id);
    print(result);
} catch (e) {
    print("Exception when calling PostsApi->postsIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the post to retrieve | [default to null]

### Return type

[**Post**](Post.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

