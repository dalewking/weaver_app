# jsonplaceholder_api.api.AlbumsApi

## Load the API package
```dart
import 'package:jsonplaceholder_api/api.dart';
```

All URIs are relative to *https://jsonplaceholder.typicode.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**albumsIdGet**](AlbumsApi.md#albumsIdGet) | **GET** /albums/{id} | Get specific album
[**albumsIdPhotosGet**](AlbumsApi.md#albumsIdPhotosGet) | **GET** /albums/{id}/photos | Get photos for a specific album
[**getAlbums**](AlbumsApi.md#getAlbums) | **GET** /albums | Get all available albums


# **albumsIdGet**
> Album albumsIdGet(id)

Get specific album

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = AlbumsApi();
var id = 56; // int | The ID of the album to retrieve

try { 
    var result = api_instance.albumsIdGet(id);
    print(result);
} catch (e) {
    print("Exception when calling AlbumsApi->albumsIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the album to retrieve | [default to null]

### Return type

[**Album**](Album.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **albumsIdPhotosGet**
> List<Photo> albumsIdPhotosGet(id)

Get photos for a specific album

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = AlbumsApi();
var id = 56; // int | post id

try { 
    var result = api_instance.albumsIdPhotosGet(id);
    print(result);
} catch (e) {
    print("Exception when calling AlbumsApi->albumsIdPhotosGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| post id | [default to null]

### Return type

[**List<Photo>**](Photo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAlbums**
> List<Album> getAlbums(id, userId)

Get all available albums

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = AlbumsApi();
var id = 56; // int | Filter by album ID
var userId = 56; // int | Filter by user ID

try { 
    var result = api_instance.getAlbums(id, userId);
    print(result);
} catch (e) {
    print("Exception when calling AlbumsApi->getAlbums: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Filter by album ID | [optional] [default to null]
 **userId** | **int**| Filter by user ID | [optional] [default to null]

### Return type

[**List<Album>**](Album.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

