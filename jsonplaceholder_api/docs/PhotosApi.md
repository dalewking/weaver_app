# jsonplaceholder_api.api.PhotosApi

## Load the API package
```dart
import 'package:jsonplaceholder_api/api.dart';
```

All URIs are relative to *https://jsonplaceholder.typicode.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPhoto**](PhotosApi.md#getPhoto) | **GET** /photos/{id} | Get specific photo
[**getPhotos**](PhotosApi.md#getPhotos) | **GET** /photos | Get all available photos


# **getPhoto**
> Photo getPhoto(id)

Get specific photo

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = PhotosApi();
var id = 56; // int | The ID of the photo to retrieve

try { 
    var result = api_instance.getPhoto(id);
    print(result);
} catch (e) {
    print("Exception when calling PhotosApi->getPhoto: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the photo to retrieve | [default to null]

### Return type

[**Photo**](Photo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPhotos**
> List<Photo> getPhotos(id, albumId)

Get all available photos

### Example 
```dart
import 'package:jsonplaceholder_api/api.dart';

var api_instance = PhotosApi();
var id = 56; // int | Filter by photo ID
var albumId = 56; // int | Filter by album ID

try { 
    var result = api_instance.getPhotos(id, albumId);
    print(result);
} catch (e) {
    print("Exception when calling PhotosApi->getPhotos: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Filter by photo ID | [optional] [default to null]
 **albumId** | **int**| Filter by album ID | [optional] [default to null]

### Return type

[**List<Photo>**](Photo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

