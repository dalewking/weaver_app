library jsonplaceholder_api.api;

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';

part 'api_client.dart';
part 'api_helper.dart';
part 'api_exception.dart';
part 'auth/authentication.dart';
part 'auth/api_key_auth.dart';
part 'auth/oauth.dart';
part 'auth/http_basic_auth.dart';

part 'api/albums_api.dart';
part 'api/comments_api.dart';
part 'api/photos_api.dart';
part 'api/posts_api.dart';
part 'api/todos_api.dart';
part 'api/users_api.dart';

part 'model/album.dart';
part 'model/comment.dart';
part 'model/new_post.dart';
part 'model/photo.dart';
part 'model/post.dart';
part 'model/todo.dart';
part 'model/user.dart';
part 'model/user_address.dart';
part 'model/user_address_geo.dart';
part 'model/user_company.dart';


ApiClient defaultApiClient = ApiClient();
