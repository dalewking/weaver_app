part of jsonplaceholder_api.api;

class Comment {
  
  int id = null;
  
  int postId = null;
  
  String name = null;
  
  String email = null;
  
  String body = null;
  Comment();

  @override
  String toString() {
    return 'Comment[id=$id, postId=$postId, name=$name, email=$email, body=$body, ]';
  }

  Comment.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    postId = json['postId'];
    name = json['name'];
    email = json['email'];
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (postId != null)
      json['postId'] = postId;
    if (name != null)
      json['name'] = name;
    if (email != null)
      json['email'] = email;
    if (body != null)
      json['body'] = body;
    return json;
  }

  static List<Comment> listFromJson(List<dynamic> json) {
    return json == null ? List<Comment>() : json.map((value) => Comment.fromJson(value)).toList();
  }

  static Map<String, Comment> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, Comment>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Comment.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Comment-objects as value to a dart map
  static Map<String, List<Comment>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<Comment>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = Comment.listFromJson(value);
       });
     }
     return map;
  }
}

