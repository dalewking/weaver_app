part of jsonplaceholder_api.api;

class UserCompany {
  
  String name = null;
  
  String catchPhrase = null;
  
  String bs = null;
  UserCompany();

  @override
  String toString() {
    return 'UserCompany[name=$name, catchPhrase=$catchPhrase, bs=$bs, ]';
  }

  UserCompany.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    name = json['name'];
    catchPhrase = json['catchPhrase'];
    bs = json['bs'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (name != null)
      json['name'] = name;
    if (catchPhrase != null)
      json['catchPhrase'] = catchPhrase;
    if (bs != null)
      json['bs'] = bs;
    return json;
  }

  static List<UserCompany> listFromJson(List<dynamic> json) {
    return json == null ? List<UserCompany>() : json.map((value) => UserCompany.fromJson(value)).toList();
  }

  static Map<String, UserCompany> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, UserCompany>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = UserCompany.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of UserCompany-objects as value to a dart map
  static Map<String, List<UserCompany>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<UserCompany>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = UserCompany.listFromJson(value);
       });
     }
     return map;
  }
}

