part of jsonplaceholder_api.api;

class NewPost {
  
  int userId = null;
  
  String title = null;
  
  String body = null;
  NewPost();

  @override
  String toString() {
    return 'NewPost[userId=$userId, title=$title, body=$body, ]';
  }

  NewPost.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    userId = json['userId'];
    title = json['title'];
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (userId != null)
      json['userId'] = userId;
    if (title != null)
      json['title'] = title;
    if (body != null)
      json['body'] = body;
    return json;
  }

  static List<NewPost> listFromJson(List<dynamic> json) {
    return json == null ? List<NewPost>() : json.map((value) => NewPost.fromJson(value)).toList();
  }

  static Map<String, NewPost> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, NewPost>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = NewPost.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of NewPost-objects as value to a dart map
  static Map<String, List<NewPost>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<NewPost>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = NewPost.listFromJson(value);
       });
     }
     return map;
  }
}

