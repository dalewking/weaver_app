part of jsonplaceholder_api.api;

class UserAddress {
  
  String street = null;
  
  String suite = null;
  
  String city = null;
  
  String zipcode = null;
  
  UserAddressGeo geo = null;
  UserAddress();

  @override
  String toString() {
    return 'UserAddress[street=$street, suite=$suite, city=$city, zipcode=$zipcode, geo=$geo, ]';
  }

  UserAddress.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    street = json['street'];
    suite = json['suite'];
    city = json['city'];
    zipcode = json['zipcode'];
    geo = (json['geo'] == null) ?
      null :
      UserAddressGeo.fromJson(json['geo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (street != null)
      json['street'] = street;
    if (suite != null)
      json['suite'] = suite;
    if (city != null)
      json['city'] = city;
    if (zipcode != null)
      json['zipcode'] = zipcode;
    if (geo != null)
      json['geo'] = geo;
    return json;
  }

  static List<UserAddress> listFromJson(List<dynamic> json) {
    return json == null ? List<UserAddress>() : json.map((value) => UserAddress.fromJson(value)).toList();
  }

  static Map<String, UserAddress> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, UserAddress>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = UserAddress.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of UserAddress-objects as value to a dart map
  static Map<String, List<UserAddress>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<UserAddress>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = UserAddress.listFromJson(value);
       });
     }
     return map;
  }
}

