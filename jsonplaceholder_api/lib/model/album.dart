part of jsonplaceholder_api.api;

class Album {
  
  int id = null;
  
  int userId = null;
  
  String title = null;
  Album();

  @override
  String toString() {
    return 'Album[id=$id, userId=$userId, title=$title, ]';
  }

  Album.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    userId = json['userId'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (userId != null)
      json['userId'] = userId;
    if (title != null)
      json['title'] = title;
    return json;
  }

  static List<Album> listFromJson(List<dynamic> json) {
    return json == null ? List<Album>() : json.map((value) => Album.fromJson(value)).toList();
  }

  static Map<String, Album> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, Album>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Album.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Album-objects as value to a dart map
  static Map<String, List<Album>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<Album>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = Album.listFromJson(value);
       });
     }
     return map;
  }
}

