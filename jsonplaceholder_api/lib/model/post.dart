part of jsonplaceholder_api.api;

class Post {
  
  int id = null;
  
  int userId = null;
  
  String title = null;
  
  String body = null;
  Post();

  @override
  String toString() {
    return 'Post[id=$id, userId=$userId, title=$title, body=$body, ]';
  }

  Post.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    userId = json['userId'];
    title = json['title'];
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (userId != null)
      json['userId'] = userId;
    if (title != null)
      json['title'] = title;
    if (body != null)
      json['body'] = body;
    return json;
  }

  static List<Post> listFromJson(List<dynamic> json) {
    return json == null ? List<Post>() : json.map((value) => Post.fromJson(value)).toList();
  }

  static Map<String, Post> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, Post>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Post.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Post-objects as value to a dart map
  static Map<String, List<Post>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<Post>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = Post.listFromJson(value);
       });
     }
     return map;
  }
}

