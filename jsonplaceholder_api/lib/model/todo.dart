part of jsonplaceholder_api.api;

class Todo {
  
  int id = null;
  
  int userId = null;
  
  String title = null;
  
  bool completed = null;
  Todo();

  @override
  String toString() {
    return 'Todo[id=$id, userId=$userId, title=$title, completed=$completed, ]';
  }

  Todo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    userId = json['userId'];
    title = json['title'];
    completed = json['completed'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (userId != null)
      json['userId'] = userId;
    if (title != null)
      json['title'] = title;
    if (completed != null)
      json['completed'] = completed;
    return json;
  }

  static List<Todo> listFromJson(List<dynamic> json) {
    return json == null ? List<Todo>() : json.map((value) => Todo.fromJson(value)).toList();
  }

  static Map<String, Todo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, Todo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Todo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Todo-objects as value to a dart map
  static Map<String, List<Todo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<Todo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = Todo.listFromJson(value);
       });
     }
     return map;
  }
}

