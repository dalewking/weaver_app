part of jsonplaceholder_api.api;

class User {
  
  int id = null;
  
  String name = null;
  
  String username = null;
  
  String email = null;
  
  String phone = null;
  
  String website = null;
  
  UserCompany company = null;
  
  UserAddress address = null;
  User();

  @override
  String toString() {
    return 'User[id=$id, name=$name, username=$username, email=$email, phone=$phone, website=$website, company=$company, address=$address, ]';
  }

  User.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    name = json['name'];
    username = json['username'];
    email = json['email'];
    phone = json['phone'];
    website = json['website'];
    company = (json['company'] == null) ?
      null :
      UserCompany.fromJson(json['company']);
    address = (json['address'] == null) ?
      null :
      UserAddress.fromJson(json['address']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (name != null)
      json['name'] = name;
    if (username != null)
      json['username'] = username;
    if (email != null)
      json['email'] = email;
    if (phone != null)
      json['phone'] = phone;
    if (website != null)
      json['website'] = website;
    if (company != null)
      json['company'] = company;
    if (address != null)
      json['address'] = address;
    return json;
  }

  static List<User> listFromJson(List<dynamic> json) {
    return json == null ? List<User>() : json.map((value) => User.fromJson(value)).toList();
  }

  static Map<String, User> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, User>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = User.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of User-objects as value to a dart map
  static Map<String, List<User>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<User>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = User.listFromJson(value);
       });
     }
     return map;
  }
}

