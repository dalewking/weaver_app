part of jsonplaceholder_api.api;

class Photo {
  
  int id = null;
  
  int albumId = null;
  
  String title = null;
  
  String url = null;
  
  String thumbnailUrl = null;
  Photo();

  @override
  String toString() {
    return 'Photo[id=$id, albumId=$albumId, title=$title, url=$url, thumbnailUrl=$thumbnailUrl, ]';
  }

  Photo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    albumId = json['albumId'];
    title = json['title'];
    url = json['url'];
    thumbnailUrl = json['thumbnailUrl'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (albumId != null)
      json['albumId'] = albumId;
    if (title != null)
      json['title'] = title;
    if (url != null)
      json['url'] = url;
    if (thumbnailUrl != null)
      json['thumbnailUrl'] = thumbnailUrl;
    return json;
  }

  static List<Photo> listFromJson(List<dynamic> json) {
    return json == null ? List<Photo>() : json.map((value) => Photo.fromJson(value)).toList();
  }

  static Map<String, Photo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, Photo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Photo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Photo-objects as value to a dart map
  static Map<String, List<Photo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<Photo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = Photo.listFromJson(value);
       });
     }
     return map;
  }
}

