part of jsonplaceholder_api.api;

class UserAddressGeo {
  
  String lat = null;
  
  String lng = null;
  UserAddressGeo();

  @override
  String toString() {
    return 'UserAddressGeo[lat=$lat, lng=$lng, ]';
  }

  UserAddressGeo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lat = json['lat'];
    lng = json['lng'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lat != null)
      json['lat'] = lat;
    if (lng != null)
      json['lng'] = lng;
    return json;
  }

  static List<UserAddressGeo> listFromJson(List<dynamic> json) {
    return json == null ? List<UserAddressGeo>() : json.map((value) => UserAddressGeo.fromJson(value)).toList();
  }

  static Map<String, UserAddressGeo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, UserAddressGeo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = UserAddressGeo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of UserAddressGeo-objects as value to a dart map
  static Map<String, List<UserAddressGeo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<UserAddressGeo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = UserAddressGeo.listFromJson(value);
       });
     }
     return map;
  }
}

