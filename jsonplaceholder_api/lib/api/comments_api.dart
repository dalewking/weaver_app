part of jsonplaceholder_api.api;



class CommentsApi {
  final ApiClient apiClient;

  CommentsApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Get specific comment with HTTP info returned
  ///
  /// 
  Future<Response> getCommentWithHttpInfo(int id) async {
    Object postBody;

    // verify required params are set
    if(id == null) {
     throw ApiException(400, "Missing required param: id");
    }

    // create path and map variables
    String path = "/comments/{id}".replaceAll("{format}","json").replaceAll("{" + "id" + "}", id.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = [];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Get specific comment
  ///
  /// 
  Future<Comment> getComment(int id) async {
    Response response = await getCommentWithHttpInfo(id);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'Comment') as Comment;
    } else {
      return null;
    }
  }

  /// Get all available comments with HTTP info returned
  ///
  /// 
  Future<Response> getCommentsWithHttpInfo({ int id, int postId }) async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/comments".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};
    if(id != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "id", id));
    }
    if(postId != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "postId", postId));
    }

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = [];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Get all available comments
  ///
  /// 
  Future<List<Comment>> getComments({ int id, int postId }) async {
    Response response = await getCommentsWithHttpInfo( id: id, postId: postId );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<Comment>') as List).map((item) => item as Comment).toList();
    } else {
      return null;
    }
  }

}
