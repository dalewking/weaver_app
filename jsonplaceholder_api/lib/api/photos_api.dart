part of jsonplaceholder_api.api;



class PhotosApi {
  final ApiClient apiClient;

  PhotosApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Get specific photo with HTTP info returned
  ///
  /// 
  Future<Response> getPhotoWithHttpInfo(int id) async {
    Object postBody;

    // verify required params are set
    if(id == null) {
     throw ApiException(400, "Missing required param: id");
    }

    // create path and map variables
    String path = "/photos/{id}".replaceAll("{format}","json").replaceAll("{" + "id" + "}", id.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = [];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Get specific photo
  ///
  /// 
  Future<Photo> getPhoto(int id) async {
    Response response = await getPhotoWithHttpInfo(id);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'Photo') as Photo;
    } else {
      return null;
    }
  }

  /// Get all available photos with HTTP info returned
  ///
  /// 
  Future<Response> getPhotosWithHttpInfo({ int id, int albumId }) async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/photos".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};
    if(id != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "id", id));
    }
    if(albumId != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "albumId", albumId));
    }

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = [];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Get all available photos
  ///
  /// 
  Future<List<Photo>> getPhotos({ int id, int albumId }) async {
    Response response = await getPhotosWithHttpInfo( id: id, albumId: albumId );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<Photo>') as List).map((item) => item as Photo).toList();
    } else {
      return null;
    }
  }

}
