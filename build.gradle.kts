import org.openapitools.generator.gradle.plugin.tasks.GenerateTask

plugins {
	id("org.openapi.generator") version("4.2.1")
}

repositories {
	mavenCentral()
}

val swaggerOutputDir = file("jsonplaceholder_api")

tasks {
	val apiGenerate by registering(GenerateTask::class) {
		inputSpec.set("jsonplaceholder-api.yaml")
		outputDir.set(swaggerOutputDir.toString())
		generatorName.set("dart")
		generateModelDocumentation.set(false)
        configOptions.put("pubName", "jsonplaceholder_api")
	}
}
